﻿using Dialogistic.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Dialogistic.DAL;

namespace Dialogistic.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminsController : Controller
    {
        #region Setup
        private ApplicationUserManager _userManager;

        private DialogisticContext db = new DialogisticContext();

        public AdminsController()
        {
        }

        public AdminsController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        #endregion

        /// <summary>
        /// Loads the dashboard for admins, grabbing important information to display.
        /// </summary>
        /// <returns>The dashboard view if the user is authenticated, and the login page if not.</returns>
        [HttpGet]
        public ActionResult Dashboard()
        {
            if (User.Identity.IsAuthenticated)
            {
                MapAddresses();
                // Get the current user
                var currentUser = User.Identity.GetUserId();
                ViewBag.UserProfile = db.UserProfiles.Where(x => x.UserID.Equals(currentUser));

                // Grab all of the calls still needing to be made
                ViewBag.TotalRemainingCalls = db.CallAssignments.Count();

                // Grab the total donations raised by all callers
                var totalDonations = db.UserProfiles.Sum(x => x.DonationsRaised).ToString();
                ViewBag.TotalDonations = String.Format("{0:c}", totalDonations);
                return View();

            }

            // If we got this far, the user isn't authenticated - send them to the login screen
            return RedirectToAction("Login", "Account");
        }


        public ActionResult HelpPage()
        {
            return View();
        }



        public JsonResult MapAddresses()
        {
            Geolocation geo = new Geolocation();
            var constituents = db.Constituents;

            List<string[]> locations = new List<string[]> { };

            foreach (var item in constituents)
            {
                string lineOne = item.PreferredAddressLine1.ToString();
                string city = item.PreferredCity.ToString();
                string state = item.PreferredState.ToString();
                string address = lineOne + " " + city + " " + state;

                geo.GetGeocode(address);
                string[] geolocation = new string[2];
                geolocation[0] = geo.Latitude;
                geolocation[1] = geo.Longitude;
                locations.Add(geolocation);
            }

            return Json(locations, JsonRequestBehavior.AllowGet);
        }

        #region Reports
        /// <summary>
        /// Gets the current yearly gifts by month 
        /// </summary>
        /// <returns>Json object with months and sum of gifts during said months</returns>
        public JsonResult CurrentYearlyReport()
        {
            GiftReports report = new GiftReports();
            return Json(report.YearlyReport(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the current yearly gifts by month 
        /// </summary>
        /// <returns>Json object with months and sum of gifts during said months</returns>
        public JsonResult CurrentMonthlyReport()
        {
            GiftReports report = new GiftReports();
            return Json(report.MonthlyReport(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the current yearly gifts by month 
        /// </summary>
        /// <returns>Json object with months and sum of gifts during said months</returns>
        public JsonResult CurrentDailyReport()
        {
            GiftReports report = new GiftReports();
            return Json(report.DailyReport(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        /// <summary>
        /// Loads the overlay with the current user's information displayed.
        /// </summary>
        /// <returns>The site overlay partial view.</returns>
        [ChildActionOnly]
        public PartialViewResult _SiteOverlay()
        {
            // Grab the current user
            var currentUserID = User.Identity.GetUserId();
            if (currentUserID != null)
            {
                // Grab their profile so we can display their information
                UserProfile user = db.UserProfiles.Where(x => x.UserID.Equals(currentUserID)).FirstOrDefault();

                // Return the partial view with the user profile
                return PartialView(user);
            }

            // If we got this far, we couldn't find a user associated with their ID - return the view without it
            return PartialView();
        }

        // POST: /Admins/LogOff
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Generates a list of call assignments of constituents associated with callers.
        /// Higher priority constituents are assigned to callers with higher accumulated donation success.
        /// The prioritization involves weighing multiple factors related to constitutent's identity and donation history.
        /// </summary>
        /// <returns>A list of CallAssignments -- Will be empty if there are no Constituents that need to be contacted</returns>
        public ActionResult GenerateCallList()
        {
            // get list of processed constituents with updated priority levels.
            List<Constituent> constituents = GetConstituents();

            // stores the total number of constitutents in the created list.
            int numberOfConstituents = constituents.Count;

            // Only build the call assigment list if there are Constituents that need to be contacted
            if (constituents.Count != 0)
            {

                // Create an empty list of Call Assignments
                List<CallAssignment> callList = new List<CallAssignment>();

                // Generate a list of callers from users designated as such sorted in descending order by total donations raised over time.
                List<UserProfile> callers = db.UserProfiles.Where(x => x.IsCaller).OrderByDescending(s => s.DonationsRaised).ToList();

                // store the number of callers in the list.
                int numberOfCallers = callers.Count;

                // determine the number of constituents to assign to each caller on an equitable basis.
                int constituentsPerCaller = numberOfConstituents / numberOfCallers;


                // order list in descending order according to CallPriority.
                constituents = constituents.OrderByDescending(s => s.CallPriority).ToList();

                // variable to keep track of number constituents assigned to a caller so far.
                int assignedCallTotal;

                // variable to keep track of how far into the constituents list we are independent of the assignments to callers.
                int constituentListIndex = 0;

                // keeps track of how deep we are into the constituent list for a given caller.
                int index;


                // This loop assigns to each caller an even number of constituents such that better callers get higher priority constituents.
                foreach (var person in callers)
                {
                    assignedCallTotal = 0;

                    index = 0;

                    // Loop through the list of Constituents, assigning a constituent to the current caller until no more can be added.                
                    foreach (var item in constituents)
                    {
                        // This checks of the caller can still be assigned constituents from further down the list.
                        if (assignedCallTotal < constituentsPerCaller && constituentListIndex == index)
                        {
                            // matches up the caller's id with the given constituent's id.
                            callList.Add(new CallAssignment() { CallerID = person.UserID, ConstituentID = item.ConstituentID });

                            // increments total to prevent over assigning constituents to one caller.
                            assignedCallTotal = assignedCallTotal + 1;

                            // increments the List index to keep track of how far into the constituents list the iterations have traversed.
                            constituentListIndex = constituentListIndex + 1;
                        }

                        // increments counter to keep track how far into list one has gotten for this caller.
                        index = index + 1;
                    }
                }

                // Add the assignments to the database, and save the changes
                db.CallAssignments.AddRange(callList);
                db.SaveChanges();

                return RedirectToAction("ManageCallList", "Admins", callList);
            }

            // Return the list -- this will be an empty list if there were no Constituents that needed to be contacted
            return RedirectToAction("ManageCallList", "Admins");
        }

        /// <summary>
        /// This is a helper method for getting and returning a list of constituents.
        /// This involves processing the constituent's and given each a calculated donation status and call priority level.
        /// </summary>
        /// <returns>A list of prioritized and classified constituents to be called.</returns>
        public List<Constituent> GetConstituents()
        {
            // Set the threshold for determining if a Constituent needs to be added to the call assignments - (from a year ago and into the past)
            DateTime contactThreshold = DateTime.Now.AddDays(-365); 

            // Build the list of Constituents that need to be called - (last contacted date > 365 days ago, or null)
            List<Constituent> constituents = db.Constituents.Where(x => x.LastContacted < contactThreshold && x.Deceased == false).ToList();
            constituents.AddRange(db.Constituents.Where(x => x.LastContacted == null && x.Deceased == false).ToList());


            // order list in ascending order according LifeTimeDonations.
            constituents.OrderBy(s => s.LifeTimeDonations);

            // obtains conunt of constituents which also provides a ranking via the constitents having been sorted in ascending order.
            int countOfConstituents = constituents.Count;

            // Prioritize list of constituents according to donation status.
            // Loop through the list of Constituents, assigning an appropriate donation status and call priority to each according to their donation history.                
            foreach (var item in constituents)
            {
                // obtain the calculated donation status and call priority for a given consitituent.
                var result = GiveConstituentRanking(item.LastContacted, item.NextToLastGiftDate, item.LastGiftDate, item.UniversityRelationship, countOfConstituents);

                // assign the resultant donation status to the constituent.
                item.DonationStatus = result.Item1;

                // assign the resultant call priority level to the constituent.
                item.CallPriority = result.Item2;

                // decrements the count of the constituents and thus the rank of the next constituent examined.
                countOfConstituents = countOfConstituents - 1;
            }


            // save the changes to the constituents to the constituents database table.
            db.SaveChanges();

            return constituents;
        }

        /// <summary>
        /// This is another helper method utilized to determining the ranking and status of a constituent
        /// based on the their donation history, relationship to the university, and relative ranking in the current list being examined.
        /// </summary>
        /// <param name="contactTime"> This comes from last contacted field of a constituent.</param>
        /// <param name="secondToLastGiftTime">This comes from the NextToLastGiftDate field of a constituent.</param>
        /// <param name="giftTime">This comes from the LastGiftDate field of a constituent </param>
        /// <param name="relationship">This comes from the University relationship field of a constituent.</param>
        /// <param name="rankingAscending">This comes from relative ranking for the currently examined list of constituents</param>
        /// <returns>This returns a tuple containing the calculated donation status and call priority level.</returns>
        public Tuple<string, int> GiveConstituentRanking(DateTime? contactTime, DateTime? secondToLastGiftTime, DateTime? giftTime, string relationship, int rankingAscending)
        {
            // initialize string to empty.
            string constituentStatus = "";

            // intialize default constituent ranking as zero.
            int constituentRanking = 0;


            /////////////// first assign priority points based on contact times and donation history//////////////////

            DateTime contactThreshold;

            // At Risk
            // Set the threshold for determining if a Constituent is at Risk - (1 year ago from today)
            contactThreshold = DateTime.Now.AddDays(-365);
            if (giftTime <= contactThreshold && giftTime != null)
            {
                constituentStatus = "At Risk";

                constituentRanking = 5;


            }

            // Lapsing
            // Set the threshold for determining if a Constituent is Lapsing - (2 years ago from today)
            contactThreshold = DateTime.Now.AddDays(-365 * 2);
            if (giftTime <= contactThreshold && giftTime != null)
            {
                constituentStatus = "Lapsing";

                constituentRanking = 3;
            }


            // Lapsed
            // Set the threshold for determining if a Constituent is Lapsed - (3 years ago from today)
            contactThreshold = DateTime.Now.AddDays(-365 * 3);
            if (giftTime <= contactThreshold && giftTime != null)
            {
                constituentStatus = "Lapsed";

                constituentRanking = 2;
            }


            // Lost
            // Set the threshold for determining if a Constituent is Lost - (5 years ago from today)
            contactThreshold = DateTime.Now.AddDays(-365 * 5);
            if (contactTime <= contactThreshold && giftTime != null)
            {
                constituentStatus = "Lost";

                constituentRanking = 1;
            }




            // Recaptured
            // Set the threshold for determining if a Constituent has been recaptured - (1 and half years between donations)
            //To get the amount of days between two gift dates.  
            if (giftTime != null && secondToLastGiftTime != null)
            { 
                int daysDiff = ((TimeSpan)(giftTime - secondToLastGiftTime)).Days;
                contactThreshold = DateTime.Now.AddDays(-365 * 1);
                if (daysDiff > 365 * 1.25 && giftTime >= contactThreshold)
                {
                     constituentStatus = "Recaptured";

                    constituentRanking = 4;
                }
             }


            // Retained
            // Set the threshold for determining if a Constituent has been retained - (less than 1 year between donations)
            //To get the amount of days between two gift dates.  
            if (giftTime != null && secondToLastGiftTime != null)
            {
                int daysDiff2 = ((TimeSpan)(giftTime - secondToLastGiftTime)).Days;
                contactThreshold = DateTime.Now.AddDays(-365 * 1);
                if (daysDiff2 < 365 * 1.25 && giftTime >= contactThreshold)
                {
                    constituentStatus = "Retained";

                    constituentRanking = 7;
                }
            }


            // Acquired
            // Set the threshold for determining if a Constituent has been acquired - (first gift ever within the last year.)
            int daysDiff3 = ((TimeSpan)(DateTime.Now - giftTime)).Days;
            if (secondToLastGiftTime == null && giftTime != null && daysDiff3 < 365)
            {
                constituentStatus = "Acquired";

                constituentRanking = 6;

            }


            // Never Donors
            // Determining if a Constituent has never donated - (no donations at all)
            if (secondToLastGiftTime == null && giftTime == null)
            {
                constituentStatus = "Never Donors";

                constituentRanking = 0;
            }

            ///////////////// Now assign additional priority points based on constituent's relation to the university.

            if (relationship != null)
            {
                /// checks for university relation of constituent and assigns appropriate value.
                if (relationship.Equals("Student"))
                {
                    constituentRanking = constituentRanking + 1;
                }
                else if (relationship.Equals("Parent"))
                {
                    constituentRanking = constituentRanking + 2;
                }
                else if (relationship.Equals("Alumni"))
                {
                    constituentRanking = constituentRanking + 3;
                }
                else if (relationship.Equals("Corporation"))
                {
                    constituentRanking = constituentRanking + 4;
                }
                else
                {
                    // we add no additional priority points
                }
            }

            ///////////////// Now assign additional priority points based on constituent's lifetime donations ranking compared to others
            ///////////////// in the current list being examined.

            constituentRanking = constituentRanking + rankingAscending;

            return Tuple.Create(constituentStatus, constituentRanking);

        }


        /// <summary>
        /// Loads the view where admins can manage which Callers are calling which Constituents.
        /// </summary>
        /// <returns>The view loaded with the current list of Call Assignments</returns>
        [HttpGet]
        public ActionResult ManageCallList()
        {
            // Build a list of the current Call Assignments
            List<CallAssignment> callList = callList = db.CallAssignments.ToList();

            // Grab all of the Users designated as Callers
            ViewBag.Callers = db.UserProfiles.Where(x => x.IsCaller);

            // Build a list of the current Call Assignments, ordered by ConstituentID
            callList = db.CallAssignments.OrderBy(x => x.ConstituentID).ToList();

            // Return the view with the current list of Call Assignments
            return View(callList);
        }

        /// <summary>
        /// Updates the provided Call Assignment with the new Caller.
        /// </summary>
        /// <param name="model">The Call Assignment to update.</param>
        /// <returns>The Manage Call List view on success, and the Dashboard on failure.</returns>
        [HttpPost]
        public ActionResult ManageCallList(CallAssignment model)
        {
            // Make sure the model is in a valid state before we do anything with it
            if (ModelState.IsValid)
            {
                // Grab the original entry from the table
                CallAssignment original = db.CallAssignments.Where(x => x.ConstituentID.Equals(model.ConstituentID)).FirstOrDefault();
                if (original != null)
                {
                    // If we found it, remove it
                    db.CallAssignments.Remove(original);

                    // Set the model's Constituent to the appropriate one from the Constituent table, based on their ID
                    model.Constituent = db.Constituents.Find(model.ConstituentID);

                    // Add the new entry to the Call Assignments table and save the changes to the database
                    db.CallAssignments.Add(model);
                    db.SaveChanges();
                }

                // Build a list of all the current Call Assignments, ordered by ConstituentID
                //List<CallAssignment> callList = new List<CallAssignment>();
                //callList = db.CallAssignments.OrderBy(x => x.ConstituentID).ToList();

                // Return this view
                return RedirectToAction("ManageCallList");
            }

            // If we got this far, the model state wasn't valid - redirect to the Dashboard
            return RedirectToAction("Dashboard", "Admins");
        }

        /// <summary>
        /// Loads the view where admins can manage Callers rolls.
        /// </summary>
        /// <returns>The view loaded with the current list of Callers</returns>
        public ActionResult ManageCallers()
        {
            // Grab all of the Users designated as Callers
            var names = db.UserProfiles.Where(x => x.IsCaller).ToList();

            // Return the view with the current list of Call Assignments
            return View(names);
        }

        /// <summary>
        /// Update a callers roll to admin and change their userprofile to no longer be a caller.
        /// </summary>
        /// <param name="userID">the ID of the user that is getting updated</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageCallers(string userID)
        {
            // Grab all of the Users designated as Callers
            var newAdmin = db.UserProfiles.Where(x => x.UserID.Equals(userID)).FirstOrDefault();

            List<UserProfile> update = db.UserProfiles.Where(x => x.UserID.Equals(userID)).ToList();
            foreach (UserProfile p in update)
            {
                p.IsCaller = false;
            }
            db.SaveChanges();

            UserManager.AddToRole(userID, "Administrator");

            var names = db.UserProfiles.Where(x => x.IsCaller).ToList();

            // Return the view with the current list of Call Assignments
            return View(names);
        }

        [HttpGet]
        public ActionResult ReviewProposedConstituentChanges()
        {
            // Create a list of all entries in the table of proposed changes
            List<ProposedConstituentsChanges> proposedChanges = db.ProposedConstituentsChanges.ToList();

            // Send the list to the view
            return View(proposedChanges);
        }

        [HttpGet]
        public ActionResult ApproveConstituentChanges(int? id)
        {
            // Make sure the model is in a valid state before trying to do anything with it
            if (id != null)
            {
                // Find the entry for this Constituent in the master Constituent table
                Constituent original = db.Constituents.Find(id);

                // Find the entry for this Constituent in the proposed changes table
                ProposedConstituentsChanges proposedChanges = db.ProposedConstituentsChanges.Find(id);

                // If we found an entry, update the values
                if (original != null)
                {
                    // Update the original values with the changes
                    db.Entry(original).CurrentValues.SetValues(proposedChanges);

                    // Remove the entry associated with this Constituent from the proposed changes table
                    db.ProposedConstituentsChanges.Remove(proposedChanges);

                    // Save the changes to the database
                    db.SaveChanges();

                    // Success - return the view containing proposed changes
                    return RedirectToAction("ReviewProposedConstituentChanges");
                }
            }

            // If we get this far, something went wrong -- return the view
            return RedirectToAction("ReviewProposedConstituentChanges");
        }

        [HttpGet]
        public ActionResult DenyConstituentChanges(int? id)
        {
            // Find the entry associated with this Constituent in the proposed changes table
            ProposedConstituentsChanges proposedChanges = db.ProposedConstituentsChanges.Find(id);

            // If we found an entry, delete it
            if (proposedChanges != null)
            {
                // Remove the entry and save the database
                db.ProposedConstituentsChanges.Remove(proposedChanges);
                db.SaveChanges();

                return RedirectToAction("ReviewProposedConstituentChanges");
            }

            // If we get this far, something went wrong -- return the view with the model
            return RedirectToAction("ReviewProposedConstituentChanges");
        }
    }
}