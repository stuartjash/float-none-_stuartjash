﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Dialogistic.DAL;
using Dialogistic.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Stripe.Models;

namespace Stripe.Controllers
{
    public class PledgeController : Controller
    {
        private DialogisticContext db = new DialogisticContext();

        /// <summary>
        /// Application DB context
        /// </summary>
        protected ApplicationDbContext ApplicationDbContext { get; set; }

        /// <summary>
        /// User manager - attached to application DB context
        /// </summary>
        protected UserManager<ApplicationUser> UserManager { get; set; }

        /// <summary>
        /// Starting Pledge controller
        /// </summary>
        public PledgeController()
        {
            this.ApplicationDbContext = new ApplicationDbContext();
            this.UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(ApplicationDbContext));
        }

        // GET: Pledge
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Runs the payment and shows success or failure upon modal completion
        /// </summary>
        /// <param name="model"></param>
        /// <param name="calls"></param>
        /// <returns></returns>
        public ActionResult Print()
        {

            List<PledgeVM> Pledges = db.CallDetails.Where(p => p.GiftType.Equals("Pledge") && p.printed.Equals(false))
                                                  .Select(c => new PledgeVM
                                                  {
                                                      Name = c.Constituent.PrimaryAddressee,
                                                      PAddress = c.Constituent.PreferredAddressLine1 + " " +
                                                                 c.Constituent.PreferredAddressLine2 + " " +
                                                                 c.Constituent.PreferredAddressLine3,
                                                      City = c.Constituent.PreferredCity,
                                                      State = c.Constituent.PreferredState,
                                                      Zip = c.Constituent.PreferredZIP,
                                                      PledgeAmmount = c.GiftAmount,
                                                      CallerName = db.UserProfiles
                                                                     .Where(x => x.UserID.Equals(c.CallerID))
                                                                     .Select(n => n.FullName).FirstOrDefault()
                                                  })
                                                  .OrderBy(x => x.Name)
                                                  .ToList();
 
            return View(Pledges);
        }
        /// <summary>
        /// Sets all current pledges in database to PRINTED status
        /// </summary>
        /// <returns>Redirects to Print page</returns>
        public ActionResult ClearPledges()
        {
            var pledges = db.CallDetails.Where(p => p.GiftType.Equals("Pledge"));

            foreach(CallDetail p in pledges)
            {
                p.printed = true;
            }
            db.SaveChanges();

            return RedirectToAction("Print");
        }

        [HttpPost]
        public async Task<ActionResult> Charge(ChargeModel model, CallDetail calls)
        {
            var result = await MakeStripeCharge(model);
            if (result.Succeeded)
            {

                var currentPledge = model.PledgeAmount;

                // constituent id passed
                int constituentID = Convert.ToInt16(Session["constituentID"].ToString());

                // returns entire constituent
                Constituent constituent = db.Constituents.SingleOrDefault(x => x.ConstituentID == constituentID);

                // Get current constituent ID and set as a string
                int callId = Convert.ToInt16(Session["constituentID"].ToString());

                // Get current caller to send to new CallDetails
                var currentUser = UserManager.FindById(User.Identity.GetUserId());
                var userProfile = db.UserProfiles.Where(x => x.UserID == currentUser.Id).FirstOrDefault();
                var caller = userProfile.UserID;

                // THIS WILL NEED REFACTORING
                if (constituent != null)
                {
                    // New call details with information
                    CallDetail callInfo = new CallDetail
                    {
                        ConstituentID = constituentID,
                        CallAnswered = true,
                        LineAvailable = true,
                        GiftType = "Credit Card",
                        GiftAmount = currentPledge,
                        DateOfCall = DateTime.Now,
                        CallerID = caller,
                        GiftRecipient = "General",
                    };
                    db.CallDetails.Add(callInfo);
                    
                    // Set the constituent information so it's updated
                    DateTime dateOnly = DateTime.Now;
                    constituent.NextToLastGiftDate = constituent.LastGiftDate;
                    constituent.LastGiftAmount = model.PledgeAmount;
                    constituent.LifeTimeDonations +=  model.PledgeAmount;
                    constituent.LastGiftDate = dateOnly.Date;
                    constituent.LastContacted = dateOnly.Date;
                    
                    db.SaveChanges();
                

                    var callDetails = db.CallDetails.Where(x => x.ConstituentID == constituentID).ToList();

                    if (callDetails != null && callDetails.Count > 0)
                    {
                        CallDetail callDetail = callDetails.FirstOrDefault(x => x.DateOfCall.ToShortDateString() == DateTime.Now.ToShortDateString());
                        if (callDetail == null)
                        {
                            callDetails[0].GiftAmount = model.PledgeAmount;
                            db.SaveChanges();
                        }
                    }
                }

                // Used to remove a constituent from the CallAssignments table and move to the next constituent
                    // CallAssignment callAssignment = db.CallAssignments.Where(x => x.ConstituentID == constituentID).FirstOrDefault();
                // Remove that constituent from the Call Assignment db
                    // db.CallAssignments.Remove(callAssignment);
                // CallAssignment nextAssignment = db.CallAssignments.Where(x => x.CallerID == currentUser.Id).FirstOrDefault();

                    // db.SaveChanges();

                ViewBag.Message = $"The pledge of {model.PledgeAmount:C} was processed successfully.  Thank you!";

                // ViewBag.NextConstituent = $"Call the next Constituent";
            }
            else
            {
                ViewBag.ErrorMessage = $"There was a problem processing your pledge";
            }
            return View("Index");
        }


        /// <summary>
        /// Runs task of making the charge
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<StripeResult> MakeStripeCharge(ChargeModel model)
        {
            var secretKey = ConfigurationManager.AppSettings["StripeSecretKey"];
            var chargeSvc = new ChargeService(secretKey);
            var options = new ChargeCreateOptions();
            options.Amount = Convert.ToInt64(model.PledgeAmount * 100m);
            options.SourceId = model.StripeToken;
            options.Currency = "usd";
            options.Description = "Pledge";
            var charge = await chargeSvc.CreateAsync(options);
            return new StripeResult
            {
                Succeeded = charge.Status == "succeeded"
            };
        }


    }

    internal class StripeResult
    {
        public bool Succeeded { get; set; }
        public string Message { get; set; }
    }
}