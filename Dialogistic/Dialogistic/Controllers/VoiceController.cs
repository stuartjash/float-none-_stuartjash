﻿using Dialogistic.Models;
using System.Collections.Generic;
using Stripe;
using Stripe.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using Twilio.TwiML;
using Twilio.TwiML.Voice;
using Dialogistic.DAL;
using Microsoft.AspNet.Identity;

namespace Dialogistic.Controllers
{
    public class VoiceController : Controller
    {
        // Instantiate an instance of our DB context file
        DialogisticContext db = new DialogisticContext();


        public ActionResult Delete(int? id)
        {
            Constituent constituent = db.Constituents.Find(id);
            CallAssignment assignment = db.CallAssignments.Where(x => x.ConstituentID == id).FirstOrDefault();

            db.CallAssignments.Remove(assignment);
            db.SaveChanges();

            return RedirectToAction("ViewCallList", "StandardUsers");
        }

        /// <summary>
        /// Builds a view for a given Constituent where a Caller can call them from.
        /// Also, checks if donation type has been specified and if so,
        /// updates the call details list with the new call detail info.
        /// </summary>
        /// <param name="id">The ID of the Constituent to build the view for.</param>
        /// <returns>The view if the there's a Constituent associated with the given ID, HttpNotFound otherwise.</returns>
        [HttpGet]
        public ActionResult Index(int? id, string donationType, string callOutcome)
        {


            // If id is null, return an error
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // It wasn't null, so try and find a Constituent associated with it
            Constituent constituent = db.Constituents.Find(id);
            Session["constituentID"] = id;

            // If we can't find a Constituent with this id, return an error
            if (constituent == null)
            {
                return HttpNotFound();
            }

           

            // check if user input has been given on potential donation type.
            if(!string.IsNullOrEmpty(donationType) || !string.IsNullOrEmpty(callOutcome))
            {

                // Check if the model state is valid before trying to do anything with it.
                if (ModelState.IsValid)
                {
                    // If it's in a valid state, create a new entry in the call details table
                    var update = new CallDetail();

                    // fill in the relevant attributes of the call details record.
                    update.CallerID = User.Identity.GetUserId();
                    update.ConstituentID = id.GetValueOrDefault();

                    DateTime dateOnly = DateTime.Now;
                    update.DateOfCall = dateOnly.Date;

                    update.CallAnswered = true;
                    update.LineAvailable = true;

                    if(!string.IsNullOrEmpty(donationType))
                    {
                        update.GiftType = donationType;
                    }
                    else
                    {
                        update.GiftType = null;
                    }
                   
                    if(!string.IsNullOrEmpty(callOutcome))
                    {
                        update.CallOutcome = callOutcome;
                    }
                    else
                    {
                        update.CallOutcome = null;
                    }
                    

                    // deal with GiftAmount and Gift recipient later.
                    
                    // update the call details table in the database.
                    db.CallDetails.Add(update);
                    db.SaveChanges();
                 
                }
            }

            // change this to viewmodel later.
            ViewBag.CallDetails = db.CallDetails.Where(x => x.ConstituentID == id).ToList().OrderBy(x => x.DateOfCall);

            // Return the view with the Consituent's information
            return View(constituent);
        }



        //[HttpPost]
        //public ActionResult Index(string txtInput, int? id)
        //{


        //    // If id is null, return an error
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    // It wasn't null, so try and find a Constituent associated with it
        //    Constituent constituent = db.Constituents.Find(id);
        //    Session["constituentID"] = id;

        //    // If we can't find a Constituent with this id, return an error
        //    if (constituent == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    // Return the view with the Consituent's information
        //    return View(constituent);

        //}




        public JsonResult GetConstituentReport(int ID)
        {
            GiftReports report = new GiftReports();
            return Json(report.GetConstituentReport(ID), JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Gets the necessary information from the form in order to have Twilio call
        /// a Constituent.
        /// </summary>
        /// <param name="to"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(string to)
        {
            // If there's a caller ID setup, grab it here
            var callerId = ConfigurationManager.AppSettings["TwilioCallerId"];

            // Instantiate a new VoiceResponse
            var response = new VoiceResponse();

            // If 'to' isn't empty or null, this is an outgoing call -- this should always have something
            if (!string.IsNullOrEmpty(to))
            {
                // Build the Dial object for placing the call
                var dial = new Dial(callerId: callerId);

                /* Wrap the phone number or Constituent name in the appropriate TwiML verb
                   by checking if the number given has only digits and format symbols */
                if (Regex.IsMatch(to, "^[\\d\\+\\-\\(\\) ]+$"))
                {
                    dial.Number(to);
                }
                else
                {
                    dial.Client(to);
                }

                // Append the caller ID
                response.Append(dial);
            }
            // 'to' was empty or null -- this is an incoming call (SHOULD NEVER HAPPEN)
            else
            {
                // Default voice playback when an incoming call is placed -- not going to be used
                response.Say("Thanks for calling!");
            }

            // Send the information to Twilio
            return Content(response.ToString(), "text/xml");
        }

        [HttpPost]
        public async Task<ActionResult> Charge(ChargeModel model)
        {
            var result = await MakeStripeCharge(model);
            if (result.Succeeded)
            {
                var currentPledge = model.PledgeAmount.ToString().FirstOrDefault();

                ViewBag.Message = $"The pledge of {model.PledgeAmount:C} was processed successfully.  Thank you!";
            }
            else
            {
                ViewBag.ErrorMessage = $"There was a problem processing your pledge";
            }
            return View("Index");
        }

        private async Task<StripeResult> MakeStripeCharge(ChargeModel model)
        {
            var secretKey = ConfigurationManager.AppSettings["StripeSecretKey"];
            var chargeSvc = new ChargeService(secretKey);
            var options = new ChargeCreateOptions();
            options.Amount = Convert.ToInt64(model.PledgeAmount * 100m);
            options.SourceId = model.StripeToken;
            options.Currency = "usd";
            options.Description = "Pledge";
            var charge = await chargeSvc.CreateAsync(options);
            return new StripeResult
            {
                Succeeded = charge.Status == "succeeded"
            };
        }
    }

    internal class StripeResult
    {
        public bool Succeeded { get; set; }
        public string Message { get; set; }
    }
}
