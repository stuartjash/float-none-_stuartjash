﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Dialogistic.DAL;
using Dialogistic.Models;
using Microsoft.AspNet.Identity;
using PagedList;



namespace Dialogistic.Controllers
{
    [Authorize(Roles = "Administrator, Standard")]
    public class ConstituentsController : Controller
    {
        //private ConstituentContext db = new ConstituentContext();
        private DialogisticContext db = new DialogisticContext();

        // GET: Constituents
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            // Set ViewBag parameters for searching and sorting
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PreferredCitySortParm = sortOrder == "PreferredCity" ? "preferredcity_desc" : "PreferredCity";
            ViewBag.PreferredStateSortParm = sortOrder == "PreferredState" ? "preferredstate_desc" : "PreferredState";
            ViewBag.ContactDateSortParm = sortOrder == "ContactDate" ? "contactdate_desc" : "ContactDate";
            ViewBag.GiftDateSortParm = sortOrder == "GiftDate" ? "giftdate_desc" : "GiftDate";

            // Set page and search string information
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            // Set the ViewBag parameter for the search string
            ViewBag.CurrentFilter = searchString;

            // Get all entries in the Constituents table
            IQueryable<Constituent> constituents;
            var currentUser = User.Identity.GetUserId();
            UserProfile standardUser = db.UserProfiles.Where(x => x.UserID == currentUser).First();



            ////////////////////////////////////////////////////////////
            //  refactor query syntax into dot synatx for LINQ later  //
            ////////////////////////////////////////////////////////////

            // This obtains whether the current user is a caller.
            bool isCaller = (from U in db.UserProfiles
                             where currentUser == U.UserID && U.IsCaller == true
                             select U.IsCaller).Any();

            // this checks if the user is a caller and should thereby view only their own constituents
            if (isCaller)
            {
                // gets constitutents that are on the user's call list only.
                constituents = from C in db.Constituents
                               join CA in db.CallAssignments on C.ConstituentID equals CA.ConstituentID
                               where CA.CallerID == standardUser.UserID
                               select C;
            }
            else // This occurs if the user is not a caller i.e. an admin and can view all constituents.
            {
                // gets all constituents.
                constituents = from s in db.Constituents
                               select s;
            }


            //
            foreach (var item in constituents)
            {
                item.DonationStatus = CheckStatus(item);
            }

            // If the search string isn't null or empty, attempt to search for that substring
            if (!String.IsNullOrEmpty(searchString))
            {
                // Search all the entries by the PrimaryAddressee field
                constituents = constituents.Where(s => s.PrimaryAddressee.Contains(searchString));

                // Set the SearchMessage based on the above results
                var searchTotal = constituents.ToList();
                if (searchTotal.Count() >= 1)
                {
                    ViewBag.CurrentSearch = searchString;
                    ViewBag.SearchMessage = "Constituents matching your search: " + searchString;
                }
                else
                {
                    ViewBag.SearchMessage = "There we no constituents matching your search: " + searchString;
                }
            }

            // Perform the correct sort based on sortOrder
            switch (sortOrder)
            {
                //case "name_desc":
                //    constituents = constituents.OrderByDescending(s => s.PrimaryAddressee);
                //    break;
                case "PreferredCity":
                    constituents = constituents.OrderBy(s => s.PreferredCity);
                    break;
                case "preferredcity_desc":
                    constituents = constituents.OrderByDescending(s => s.PreferredCity);
                    break;
                case "PreferredState":
                    constituents = constituents.OrderBy(s => s.PreferredState);
                    break;
                case "preferredstate_desc":
                    constituents = constituents.OrderByDescending(s => s.PreferredState);
                    break;
                case "ContactDate":
                    constituents = constituents.OrderBy(s => s.LastContacted);
                    break;
                case "contactdate_desc":
                    constituents = constituents.OrderByDescending(s => s.LastContacted);
                    break;
                case "GiftDate":
                    constituents = constituents.OrderBy(s => s.LastGiftDate);
                    break;
                case "giftdate_desc":
                    constituents = constituents.OrderByDescending(s => s.LastGiftDate);
                    break;
                default:  // ConstituentID ascending 
                    constituents = constituents.OrderBy(s => s.ConstituentID);
                    break;
            }

            // Set the pagination parameters and return the view
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            // set view flag for indicating no error has occured.
            ViewBag.flag = "Success";

            

            return View(constituents.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateConstituent(Constituent constituent)
        {
            if (ModelState.IsValid)
            {
                db.Constituents.Add(constituent);
                db.SaveChanges();

                return RedirectToAction("Index", "Constituents");
            }

            return View(constituent);
        }

        [HttpPost]
        public bool Delete(int id)
        {
            // If this constituent is in the call assignments, remove them from there before deleting
            CallAssignment callAssignment = db.CallAssignments.Where(x => x.ConstituentID == id).FirstOrDefault();
            if (callAssignment != null)
            {
                db.CallAssignments.Remove(callAssignment);

                // Remove the constituent from the database
                Constituent constituent = db.Constituents.Where(x => x.ConstituentID == id).FirstOrDefault();
                if (constituent != null)
                {
                    db.Constituents.Remove(constituent);
                    db.SaveChanges();
                }
                return true;
            }
            else
            {
                // Remove the constituent from the database
                Constituent constituent = db.Constituents.Where(x => x.ConstituentID == id).FirstOrDefault();
                if (constituent != null)
                {
                    db.Constituents.Remove(constituent);
                    db.SaveChanges();
                }
                return true;
            }
        }

        [HttpGet]
        public ActionResult Update(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Constituent constituent = db.Constituents.Find(id);
            if (constituent == null)
            {
                return HttpNotFound();
            }

            return View(constituent);
        }

        [HttpPost]
        public ActionResult UpdateConstituent(Constituent model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", "Constituents");
        }

        /// <summary>
        /// Attempts to find a Constituent associated with the given id, and loads their information
        /// into the view if one is found.
        /// </summary>
        /// <param name="id">The id of the Constituent to look up.</param>
        /// <returns>The update view if a match is found, else an appropriate error form.</returns>
        [HttpGet]
        public ActionResult ProposeConstituentChanges(int? id)
        {
            // If the id parameter is null, give a "bad request" error
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // The parameter wasn't null -- use it to try and find the associated Constituent
            Constituent constituent = db.Constituents.Find(id);

            // If there wasn't a match with this id, give a "not found" error
            if (constituent == null)
            {
                return HttpNotFound();
            }

            // If we get this far, it means we found a match with this id -- return the view with the Constituent
            return View(constituent);
        }

        /// <summary>
        /// Creates an entry in the ProposedConstituentsChanges table, allowing non-administrators
        /// to submit changes to existing Constituents. Changes are reviewed and either accepted or
        /// denied by administrators in another controller / method.
        /// </summary>
        /// <param name="model">The Constituent to submit changes for.</param>
        /// <returns>The Constituents Index page on success, else the same view with any error messages.</returns>
        [HttpPost]
        public ActionResult ProposeConstituentChanges(Constituent model)
        {
            // Check if the model state is valid before trying to do anything with it
            if (ModelState.IsValid)
            {
                // If it's in a valid state, create a new entry in the proposed changes table
                var proposedChanges = new ProposedConstituentsChanges
                {
                    ConstituentID = model.ConstituentID,
                    PrimaryAddressee = model.PrimaryAddressee,
                    PreferredAddressLine1 = model.PreferredAddressLine1,
                    PreferredAddressLine2 = model.PreferredAddressLine2,
                    PreferredAddressLine3 = model.PreferredAddressLine3,
                    PreferredCity = model.PreferredCity,
                    PreferredState = model.PreferredState,
                    PreferredZIP = model.PreferredZIP,
                    PhoneNumber = model.PhoneNumber,
                    MobilePhoneNumber = model.MobilePhoneNumber,
                    AlternatePhoneNumber = model.AlternatePhoneNumber,
                    LastContacted = model.LastContacted,
                    NextToLastGiftDate = model.NextToLastGiftDate,
                    LastGiftDate = model.LastGiftDate,
                    Deceased = model.Deceased,
                    LastGiftAmount = model.LastGiftAmount,
                    LifeTimeDonations = model.LifeTimeDonations,
                    DonationStatus = model.DonationStatus,
                    UniversityRelationship = model.UniversityRelationship,
                    CallPriority = model.CallPriority
                };

                db.ProposedConstituentsChanges.Add(proposedChanges);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            // If we got this far, something went wrong -- return the view with the model
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string CheckStatus(Constituent constituent)
        {
            string status;
            int timeLapsed;
            DateTime current = DateTime.Now;

            if (constituent.LastGiftDate != null)
            {
                DateTime lastGift = (DateTime)constituent.LastGiftDate;
                timeLapsed = ((current.Year - lastGift.Year) * 12) + current.Month - lastGift.Month;

                if (timeLapsed >= 12 && timeLapsed <= 15)
                    status = "atRisk";
                else if (timeLapsed > 15 && timeLapsed <= 24)
                    status = "lapsing";
                else if (timeLapsed > 24 && timeLapsed <= 60)
                    status = "lapsed";
                else if (timeLapsed > 60)
                    status = "lost";
                else
                    status = "retained";
            }
            else
            {
                status = "neverDonor";
            }

            return status;
        }
    }
}
