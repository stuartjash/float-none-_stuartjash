﻿using Dialogistic.DAL;
using Dialogistic.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dialogistic.Controllers
{
    [Authorize(Roles = "Standard")]
    public class StandardUsersController : Controller
    {
        #region Set up
        private ApplicationUserManager _userManager;

        private DialogisticContext db = new DialogisticContext();

        public StandardUsersController()
        {
        }

        public StandardUsersController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        #endregion

        /// <summary>
        /// Loads the Dashboard for Standard Users.
        /// </summary>
        /// <returns>The Dashboard view for Standard Users.</returns>
        [HttpGet]
        public ActionResult Dashboard()
        {
            if (User.Identity.IsAuthenticated)
            {
                var currentUser = User.Identity.GetUserId();
                UserProfile standardUser = db.UserProfiles.Where(x => x.UserID == currentUser).First();
                standardUser.CallsRemaining = db.CallAssignments.Where(x => x.CallerID == standardUser.UserID).Count();
           
                return View(standardUser);
            }


            return RedirectToAction("Login", "Account");
        }


        public ActionResult HelpPage()
        {
            return View();
        }


        /// <summary>
        /// Gets the current yearly gifts by month 
        /// </summary>
        /// <returns>Json object with months and sum of gifts during said months</returns>
        public JsonResult CurrentYearlyReport()
        {
            GiftReports report = new GiftReports();
            return Json(report.YearlyReport(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the current yearly gifts by month 
        /// </summary>
        /// <returns>Json object with months and sum of gifts during said months</returns>
        public JsonResult CurrentMonthlyReport()
        {
            GiftReports report = new GiftReports();
            return Json(report.MonthlyReport(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the current yearly gifts by month 
        /// </summary>
        /// <returns>Json object with months and sum of gifts during said months</returns>
        public JsonResult CurrentDailyReport()
        {
            GiftReports report = new GiftReports();
            return Json(report.DailyReport(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Shows the list of Constituents a Caller is responsible for calling.
        /// </summary>
        /// <returns>The view with any call assignments assigned to a Caller.</returns>
        [HttpGet]
        public ActionResult ViewCallList()
        {
            var currentUser = User.Identity.GetUserId();
            UserProfile standardUser = db.UserProfiles.Where(x => x.UserID == currentUser).First();
            List<CallAssignment> callAssignments = db.CallAssignments.Where(x => x.CallerID == standardUser.UserID).ToList();

            return View(callAssignments);
        }

        /// <summary>
        /// Loads the site overlay for Standard Users.
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult _SiteOverlay()
        {
            var currentUserID = User.Identity.GetUserId();
            if (currentUserID != null)
            {
                UserProfile user = db.UserProfiles.Where(x => x.UserID.Equals(currentUserID)).FirstOrDefault();

                return PartialView(user);
            }

            return PartialView();
        }

        /// <summary>
        /// Allows Standard Users to log out of their account from the sidebar link.
        /// </summary>
        /// <returns>Sends the user to the log in view after logging them out.</returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }
    }
}