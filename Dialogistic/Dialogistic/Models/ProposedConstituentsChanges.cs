namespace Dialogistic.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProposedConstituentsChanges
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ConstituentID { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Primary Addressee")]
        public string PrimaryAddressee { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Primary Address")]
        public string PreferredAddressLine1 { get; set; }

        [StringLength(100)]
        [Display(Name = "Address 2")]
        public string PreferredAddressLine2 { get; set; }

        [StringLength(100)]
        [Display(Name = "Address 3")]
        public string PreferredAddressLine3 { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "City")]
        public string PreferredCity { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "State")]
        public string PreferredState { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "ZIP")]
        public string PreferredZIP { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        [Display(Name = "Mobile Number")]
        public string MobilePhoneNumber { get; set; }

        [StringLength(50)]
        [Display(Name = "Alt. Number")]
        public string AlternatePhoneNumber { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Last Contacted")]
        public DateTime? LastContacted { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Next-to-Last Gift Date")]
        public DateTime? NextToLastGiftDate { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Last Gift Date")]
        public DateTime? LastGiftDate { get; set; }

        public bool Deceased { get; set; }

        [Display(Name = "Last Gift Amount")]
        public decimal? LastGiftAmount { get; set; }

        [Display(Name = "Lifetime Donations")]
        public decimal? LifeTimeDonations { get; set; }

        [StringLength(20)]
        [Display(Name = "Donation Status")]
        public string DonationStatus { get; set; }

        [StringLength(20)]
        [Display(Name = "University Relationship")]
        public string UniversityRelationship { get; set; }

        [Display(Name = "Call Priority")]
        public int? CallPriority { get; set; }
    }
}
