namespace Dialogistic.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CallDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CallDetail()
        {
            CallAssignments = new HashSet<CallAssignment>();
        }

        [Key]
        public int CallID { get; set; }

        [Required]
        [StringLength(128)]
        public string CallerID { get; set; }

        public int ConstituentID { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateOfCall { get; set; }

        public bool CallAnswered { get; set; }

        public bool LineAvailable { get; set; }
        public bool printed { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Gift Amount")]
        public decimal? GiftAmount { get; set; }

        [StringLength(256)]
        [Display(Name = "Gift Type")]
        public string GiftType { get; set; }

        [StringLength(256)]
        [Display(Name = "Gift Recipient")]
        public string GiftRecipient { get; set; }


        [StringLength(128)]
        [Display(Name = "Call Outcome")]
        public string CallOutcome { get; set; }




        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CallAssignment> CallAssignments { get; set; }

        public virtual Constituent Constituent { get; set; }
    }
}
