namespace Dialogistic.Models
{
    using LINQtoCSV;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Constituent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Constituent()
        {
            CallAssignments = new HashSet<CallAssignment>();
            CallDetails = new HashSet<CallDetail>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [CsvColumn(FieldIndex = 1)]
        public int ConstituentID { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Primary Addressee")]
        [CsvColumn(FieldIndex = 2)]
        public string PrimaryAddressee { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Preferred Address")]
        [CsvColumn(FieldIndex = 3)]
        public string PreferredAddressLine1 { get; set; }

        [StringLength(100)]
        [Display(Name = "Address 2")]
        [CsvColumn(FieldIndex = 4)]
        public string PreferredAddressLine2 { get; set; }

        [StringLength(100)]
        [Display(Name = "Address 3")]
        [CsvColumn(FieldIndex = 5)]
        public string PreferredAddressLine3 { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "City")]
        [CsvColumn(FieldIndex = 6)]
        public string PreferredCity { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "State")]
        [CsvColumn(FieldIndex = 7)]
        public string PreferredState { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "ZIP")]
        [CsvColumn(FieldIndex = 8)]
        public string PreferredZIP { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Phone Number")]
        [CsvColumn(FieldIndex = 9)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        [Display(Name = "Mobile Number")]
        [CsvColumn(FieldIndex = 10)]
        public string MobilePhoneNumber { get; set; }

        [StringLength(50)]
        [Display(Name = "Alt. Number")]
        [CsvColumn(FieldIndex = 11)]
        public string AlternatePhoneNumber { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Last Contacted")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [CsvColumn(FieldIndex = 12)]
        public DateTime? LastContacted { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Next-to-Last Gift Date")]
        [CsvColumn(FieldIndex = 13)]
        public DateTime? NextToLastGiftDate { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Last Gift Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [CsvColumn(FieldIndex = 14)]
        public DateTime? LastGiftDate { get; set; }

        [CsvColumn(FieldIndex = 15)]
        public bool Deceased { get; set; }

        [Display(Name = "Last Gift Amount")]
        [CsvColumn(FieldIndex = 16)]
        public decimal? LastGiftAmount { get; set; }

        [Display(Name = "Lifetime Donations")]
        [CsvColumn(FieldIndex = 17)]
        public decimal? LifeTimeDonations { get; set; }

        [StringLength(20)]
        [Display(Name = "Donation Status")]
        [CsvColumn(FieldIndex = 18)]
        public string DonationStatus { get; set; }

        [StringLength(20)]
        [Display(Name = "University Relationship")]
        [CsvColumn(FieldIndex = 19)]
        public string UniversityRelationship { get; set; }

        [Display(Name = "Call Priority")]
        [CsvColumn(FieldIndex = 20)]
        public int? CallPriority { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CallAssignment> CallAssignments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CallDetail> CallDetails { get; set; }
    }
}
