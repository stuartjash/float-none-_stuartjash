﻿namespace Dialogistic.Models
{
    public partial class ReviewChangesModel
    {
        public Constituent Constituent { get; set; }
        public ProposedConstituentsChanges ConstituentChanges { get; set; }        
    }
}