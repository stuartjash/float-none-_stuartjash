﻿
// Extends jquerry to return json type from controller
jQuery.extend({
    getValues: function (url) {
        var result = null;
        $.ajax({
            url: url,
            type: 'get',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                result = data;
            }
        });
        return result;
    }
});

// Get user theme
$(document).ready(function () {
    console.log("ready!");
    var theme = $.getValues("/Account/GetTheme");
    console.log(theme);
    if (theme == "Standard") {
        $('head').append('<link rel="stylesheet" href="/Content/theme/css/standard.css">');
    }

    else if (theme == "Dark") {
        $('head').append('<link rel="stylesheet" href="/Content/theme/css/dark.css">');
    }

    $("#loadOverlay").css("display", "none");
});