
-----------------------------------------
----                                   --
-------- Stuart's insert statements ------
----                                   --
-------------------------------------------

----INSERT INTO [dbo].[UserProfiles] (UserID, UserName, FullName, Email, IsCaller, DonationsRaised) VALUES
----('4ffe9255-624b-4808-8a04-b4c18e104fc9', 'userOne', 'User One', 'user_one@gmail.com', 1, 345.20),
----('9f297b78-aa50-4ea9-91df-73486ca227da', 'userTwo', 'User Two', 'user_two@gmail.com', 1, 128.50),
------('eaf837cc-8a6a-4b65-adcd-aeae6ad1ad33', 'DialogisticSuperAdmin', 'Admin', 'dinkster01@hotmail.com', 1, 228.75),
----('f7011b1f-fdd0-4b5b-9be2-5187ff8b8dc1', 'adminUser', 'Admin User', 'admin_user@gmail.com', 0, 0);

----INSERT INTO [dbo].[Constituents](ConstituentID, PrimaryAddressee, PreferredAddressLine1, PreferredAddressLine2, PreferredAddressLine3, PreferredCity, PreferredState, PreferredZIP, PhoneNumber, MobilePhoneNumber, AlternatePhoneNumber, LastContacted, NextToLastGiftDate, LastGiftDate, Deceased, LastGiftAmount, LifeTimeDonations, DonationStatus, UniversityRelationship, CallPriority)
----	VALUES
----( 679, 'Mr. Joan Lawrence', '4778 N Orchard St', '', '', 'Fresno', 'Oregon', '97333-2274', '515-555-5516', '515-555-3843', NULL,'4/11/2019','2/25/2018' ,'10/25/2017', 1, NULL, 1000.00, NULL, 'Alumni', NULL),
----( 760, 'Ms. Sharelle Beck', 'PO Box 305', '', '', 'Monmouth', 'California', '98512-8108', '831-555-9748', NULL, NULL,'4/11/2019','2/25/2018','12/14/2017', 1, NULL, NULL, NULL, 'Alumni', NULL),
----( 778, 'Ms. Katrina Horning', '8639 Highland Rd', '', '', 'Independence', 'Ohio', '97601-9313', '541-555-9601', NULL, NULL, '4/11/2019','2/25/2018','12/8/2017', 1, NULL, NULL, NULL, 'Alumni', NULL),
----( 1013, 'Mr. Keith Hogan', '1840 W 14th Ave', '', '', 'Eugene', 'Oregon', '97086-5029', '503-555-3755', NULL, NULL,'4/11/2019','2/25/2018','12/8/2017', 1, NULL, NULL, NULL, 'Alumni', NULL),
----( 1176, 'Mr. Christian Sanchez', '135 Kanuku Ct SE', '', '', 'Salem', 'Oregon', '93314-7704', '503-555-1192', NULL, NULL, '4/11/2019','2/25/2018','12/15/2017', 1, NULL, NULL, NULL, 'Alumni', NULL),
----( 1719, 'Mr. Dell Peterson', '1471 Burns St', '', '', 'West Linn', 'Oregon', '95126-4817', '503-555-1701', NULL, NULL,'4/11/2019','2/25/2018','10/25/2017', 0, NULL, NULL, NULL, 'Alumni', NULL),
----( 1999, 'Mr. Harold McCabe', '15744 SE Harrison St', '', '', 'Portland', 'Oregon', '97031-1994', '503-555-2297', NULL, NULL,'4/11/2019','2/25/2018','12/11/2017', 0, NULL, NULL, NULL, 'Alumni', NULL),
----( 2000, 'Mr. Denny Malone', '345 7th Ave', '', '', 'New York', 'New York', '10001-5003', '503-555-2297', NULL, NULL,'4/11/2019','2/25/2018','12/11/2017', 0, NULL, 1000.00, NULL, 'Alumni', NULL),
----( 2005, 'Mr. Phil Russo', '345 7th Ave', '', '', 'New York', 'New York', '10001-5003', '503-555-2297', NULL, NULL,'4/11/2019','2/25/2018','12/11/2017', 0, NULL, 1000.00, NULL, 'Alumni', NULL);

--INSERT INTO [dbo].[CallAssignments](CallerID, ConstituentID) VALUES
--('4ffe9255-624b-4808-8a04-b4c18e104fc9', 760),
--('4ffe9255-624b-4808-8a04-b4c18e104fc9', 1013),
--('4ffe9255-624b-4808-8a04-b4c18e104fc9', 1176),
--('9f297b78-aa50-4ea9-91df-73486ca227da', 679),
--('9f297b78-aa50-4ea9-91df-73486ca227da', 778),
--('9f297b78-aa50-4ea9-91df-73486ca227da', 1719),
--('9f297b78-aa50-4ea9-91df-73486ca227da', 2000),
--('9f297b78-aa50-4ea9-91df-73486ca227da', 2005);


--INSERT INTO [dbo].[CallDetails] (CallerID, ConstituentID, DateOfCall, CallAnswered, Printed, LineAvailable, GiftType, GiftAmount, GiftRecipient)
--	VALUES ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 778, '2019/01/05', 1, 0, 1, 'Pledge', 200.00, 'Foundation'),
--		   ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 1176, '2019/01/05', 1, 0, 1, 'Pledge', 200.00, 'Art'),
--		   ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 760, '2019/01/06', 1, 0, 1, 'Pledge', 200.00, 'Science'),
--		   ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 778, '2019/01/07', 1, 0, 1, 'Pledge', 200.00, 'Sports'),
--		   ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 1999, '2019/02/08', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 1176, '2019/02/08', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 760, '2019/02/10', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 679, '2019/03/11', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 1176, '2019/03/11', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('4ffe9255-624b-4808-8a04-b4c18e104fc9', 760, '2019/03/15', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 1013, '2019/03/15', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 1176, '2019/03/20', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 760, '2019/03/20', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 778, '2019/03/21', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 679, '2019/03/23', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 1013, '2019/03/08', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 679, '2019/03/08', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 760, '2019/04/01', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 1013, '2019/04/02', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 1176, '2019/04/03', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 778, '2019/04/04', 1, 0, 1, 'Pledge', 200.00, 'General'),
--		   ('9f297b78-aa50-4ea9-91df-73486ca227da', 1176, '2019/04/10', 1, 0, 1, 'Pledge', 600.00, 'General');

