namespace Dialogistic.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Dialogistic.Models;

    public partial class DialogisticContext : DbContext
    {
        public DialogisticContext()
            : base("name=DialogisticContext")
        {
        }

        public virtual DbSet<CallAssignment> CallAssignments { get; set; }
        public virtual DbSet<CallDetail> CallDetails { get; set; }
        public virtual DbSet<Constituent> Constituents { get; set; }
        public virtual DbSet<ProposedConstituentsChanges> ProposedConstituentsChanges { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CallDetail>()
                .Property(e => e.GiftAmount)
                .HasPrecision(19, 2);

            modelBuilder.Entity<CallDetail>()
                .HasMany(e => e.CallAssignments)
                .WithOptional(e => e.CallDetail)
                .HasForeignKey(e => e.CallDetailsID);

            modelBuilder.Entity<Constituent>()
                .Property(e => e.LastGiftAmount)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Constituent>()
                .Property(e => e.LifeTimeDonations)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Constituent>()
                .HasMany(e => e.CallAssignments)
                .WithRequired(e => e.Constituent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Constituent>()
                .HasMany(e => e.CallDetails)
                .WithRequired(e => e.Constituent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProposedConstituentsChanges>()
                .Property(e => e.LastGiftAmount)
                .HasPrecision(19, 2);

            modelBuilder.Entity<ProposedConstituentsChanges>()
                .Property(e => e.LifeTimeDonations)
                .HasPrecision(19, 2);

            modelBuilder.Entity<UserProfile>()
                .Property(e => e.DonationsRaised)
                .HasPrecision(19, 2);
        }
    }
}
