﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dialogistic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dialogistic.Models.Tests
{
    [TestClass()]
    public class GeolocationTests
    {
        [TestMethod()]
        public void GetGeocodeTest()
        {
            Geolocation geo = new Geolocation();
            string address = "345 Monmouth Ave N Monmouth OR";

            string correctLat = "44.851955";
            string correctLng = "-123.237317";

            geo.GetGeocode(address);

            Assert.AreEqual(correctLat, geo.Latitude);
            Assert.AreEqual(correctLng, geo.Longitude);
        }
    }
}