﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Dialogistic.Controllers;
using System.Web.Mvc;

namespace DialogisticTests
{
    [TestClass]
    public class ConstituentsControllerTest
    {
        [TestMethod]
        public void Index_ShouldReturnViewWithCorrectFields_Success()
        {

            // Arrange
            ConstituentsController controller = new ConstituentsController();
            string sortOrder = "";
            string currentFilter = "";
            string searchString = "";
            int? page = 0;

            //// Act
            ViewResult result = controller.Index(sortOrder, currentFilter, searchString, page) as ViewResult;

            //// Assert
            Assert.AreEqual("Success", result.ViewBag.flag);

        }
    }
}
