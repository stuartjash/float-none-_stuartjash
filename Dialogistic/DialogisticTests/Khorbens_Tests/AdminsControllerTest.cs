﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Dialogistic.Controllers;

namespace DialogisticTests
{
    [TestClass]
    public class AdminsControllerTest
    {

        //[Feature, functionality or unit of work under test]_[Behavior, with a verb in there, i.e. "should" or "shouldn't" or returns]_[Expected result].
        [TestMethod]
        public void GiveConstituentRanking_ShouldWhenGivenOldDateValuesReturn_Lost()
        {
            // arrange --> creating an instance of the AdminsController class so we can access its methods
            AdminsController controller = new AdminsController();

            DateTime? contactTime = Convert.ToDateTime("12/25/2008");

            DateTime? secondToLastGiftTime = Convert.ToDateTime("12/25/2006");

            DateTime? giftTime = Convert.ToDateTime("12/25/2008");

            string relationship = "Student";

            int rankingAscending = 10;

            // act --> input parameter values into GiveConstituentRanking(...) method of controller.
            Tuple<string, int> result = controller.GiveConstituentRanking(contactTime, secondToLastGiftTime, giftTime, relationship, rankingAscending);

            // assert --> test that constituent status and constituent ranking have the correct values.
            Assert.AreEqual(result.Item1, "Lost");   //constituent status

            Assert.AreEqual(result.Item2, 12);   //constituent ranking

        }

        [TestMethod]
        public void GiveConstituentRanking_ShouldWhenGivenInputValuesInRetainRangeReturn_Retained()
        {
            // arrange --> creating an instance of the AdminsController class so we can access its methods
            AdminsController controller = new AdminsController();

            DateTime? contactTime = Convert.ToDateTime("12/25/2018");

            DateTime? secondToLastGiftTime = Convert.ToDateTime("12/25/2017");

            DateTime? giftTime = Convert.ToDateTime("12/25/2018");

            string relationship = "Student";

            int rankingAscending = 10;

            // act --> input parameter values into GiveConstituentRanking(...) method of controller.
            Tuple<string, int> result = controller.GiveConstituentRanking(contactTime, secondToLastGiftTime, giftTime, relationship, rankingAscending);

            // assert --> test that constituent status and constituent ranking have the correct values.

            Assert.AreEqual(result.Item1, "Retained");  //constituent status                               

           // Assert.AreEqual(result.Item2, 12);   //constituent ranking

        }


        [TestMethod]
        public void GiveConstituentRanking_ShouldWhenGivenInputValuesInRecapturedRangeReturn_Recaptured()
        {
            // arrange --> creating an instance of the AdminsController class so we can access its methods
            AdminsController controller = new AdminsController();

            DateTime? contactTime = Convert.ToDateTime("12/25/2018");

            DateTime? secondToLastGiftTime = Convert.ToDateTime("12/25/2016");

            DateTime? giftTime = Convert.ToDateTime("12/25/2018");

            string relationship = "Student";

            int rankingAscending = 10;

            // act --> input parameter values into GiveConstituentRanking(...) method of controller.
            Tuple<string, int> result = controller.GiveConstituentRanking(contactTime, secondToLastGiftTime, giftTime, relationship, rankingAscending);

            // assert --> test that constituent status and constituent ranking have the correct values.

            Assert.AreEqual(result.Item1, "Recaptured"); //constituent status                                  

          //  Assert.AreEqual(result.Item2, 12);   //constituent ranking

        }

        [TestMethod]
        public void GiveConstituentRanking_ShouldWhenGivenInputValuesInLapsingRangeReturn_Lapsing()
        {
            // arrange --> creating an instance of the AdminsController class so we can access its methods
            AdminsController controller = new AdminsController();

            DateTime? contactTime = Convert.ToDateTime("12/25/2018");

            DateTime? secondToLastGiftTime = Convert.ToDateTime("12/25/2016");

            DateTime? giftTime = Convert.ToDateTime("12/25/2016");

            string relationship = "Student";

            int rankingAscending = 10;

            // act --> input parameter values into GiveConstituentRanking(...) method of controller.
            Tuple<string, int> result = controller.GiveConstituentRanking(contactTime, secondToLastGiftTime, giftTime, relationship, rankingAscending);

            // assert --> test that constituent status and constituent ranking have the correct values.

            Assert.AreEqual(result.Item1, "Lapsing"); //constituent status



            //Assert.AreEqual(result.Item1, "Lapsed");
            //Assert.AreEqual(result.Item1, "At Risk");
            //Assert.AreEqual(result.Item1, "Never Donors");
            //Assert.AreEqual(result.Item1, "Acquired");                                       

            //  Assert.AreEqual(result.Item2, 12);   //constituent ranking

        }



        [TestMethod]
        public void GiveConstituentRanking_ShouldWhenGivenInputValuesInLapsedRangeReturn_Lapsed()
        {
            // arrange --> creating an instance of the AdminsController class so we can access its methods
            AdminsController controller = new AdminsController();

            DateTime? contactTime = Convert.ToDateTime("12/25/2018");

            DateTime? secondToLastGiftTime = Convert.ToDateTime("12/25/2010");

            DateTime? giftTime = Convert.ToDateTime("12/25/2015");

            string relationship = "Student";

            int rankingAscending = 10;

            // act --> input parameter values into GiveConstituentRanking(...) method of controller.
            Tuple<string, int> result = controller.GiveConstituentRanking(contactTime, secondToLastGiftTime, giftTime, relationship, rankingAscending);

            // assert --> test that constituent status and constituent ranking have the correct values.

            Assert.AreEqual(result.Item1, "Lapsed"); //constituent status



            //Assert.AreEqual(result.Item1, "Lapsed");
            //Assert.AreEqual(result.Item1, "At Risk");
            //Assert.AreEqual(result.Item1, "Never Donors");
            //Assert.AreEqual(result.Item1, "Acquired");                                       

            //  Assert.AreEqual(result.Item2, 12);   //constituent ranking

        }




        [TestMethod]
        public void GiveConstituentRanking_ShouldWhenGivenInputValuesInAtRiskRangeReturn_AtRisk()
        {
            // arrange --> creating an instance of the AdminsController class so we can access its methods
            AdminsController controller = new AdminsController();

            DateTime? contactTime = Convert.ToDateTime("12/25/2018");

            DateTime? secondToLastGiftTime = Convert.ToDateTime("12/25/2010");

            DateTime? giftTime = Convert.ToDateTime("2/14/2018");

            string relationship = "Student";

            int rankingAscending = 10;

            // act --> input parameter values into GiveConstituentRanking(...) method of controller.
            Tuple<string, int> result = controller.GiveConstituentRanking(contactTime, secondToLastGiftTime, giftTime, relationship, rankingAscending);

            // assert --> test that constituent status and constituent ranking have the correct values.

            Assert.AreEqual(result.Item1, "At Risk"); //constituent status




            //Assert.AreEqual(result.Item1, "At Risk");
            //Assert.AreEqual(result.Item1, "Never Donors");
            //Assert.AreEqual(result.Item1, "Acquired");                                       

            //  Assert.AreEqual(result.Item2, 12);   //constituent ranking

        }


     



   


    }
}
