﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dialogistic.Controllers;
using Stripe.Controllers;

namespace DialogisticTests
{
    [TestClass]
    public class PledgeControllerTest
    {
        /// <summary>
        /// Test Payment
        /// Return False
        /// </summary>
        [TestMethod]
        public void PaymentSuccessful_PaymentCompletedSuccess_False()
        {
            PledgeController pledgeController = new PledgeController();
        }

        /// <summary>
        /// Test Payment
        /// Return True
        /// </summary>
        [TestMethod]
        public void PaymentSuccessful_PaymentCompletedSuccess_True()
        {
            PledgeController pledgeController = new PledgeController();
        }
    }
}
