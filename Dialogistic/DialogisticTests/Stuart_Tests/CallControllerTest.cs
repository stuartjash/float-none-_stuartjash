﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Twilio.AspNet.Mvc;

namespace Dialogistic.Controllers
{
    /// <summary>
    /// Summary description for TwilioTest
    /// </summary>
    [TestClass()]
    public class CallControllerTest
    {
        [TestMethod()]
        public void CallTest()
        {
            CallController call = new CallController();

            var connection = (TwiMLResult)call.Connect("+15417403166");

            Assert.IsInstanceOfType(connection, typeof(TwiMLResult));

            //var result = Assert.IsType(connection, ActionResult);

           // Assert.AreEqual(connection.ExecuteResult);

            //Assert.AreEqual(connection.Data.Document, "<Response>< Dial >< Number > +15417403166 </ Number ></ Dial ></ Response >");
        }
    }
}
