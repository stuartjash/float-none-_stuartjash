﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DialogisticTests.Stuart_Tests
{
    [TestClass]
    public class LoginTests
    {
        [TestMethod]
        public void PenTestPageIsVisible_ReturnTrueIfPagesMatch_Success()
        {
            IWebDriver driverGC = new ChromeDriver();

            // Open page url
            driverGC.Navigate().GoToUrl("https://floatnone-stuartjash.azurewebsites.net/Account/Login");
            driverGC.Manage().Window.Maximize();

            // Add the email and password of the admin (page only visible to admin)
            driverGC.FindElement(By.Id("Email")).SendKeys("adminUser");
            driverGC.FindElement(By.Id("Password")).SendKeys("I_Am_Admin_User");
            driverGC.FindElement(By.Id("Password")).SendKeys(Keys.Enter);

            // Click on the Settings then View (the) Report
            driverGC.FindElement(By.LinkText("Settings")).Click();
            driverGC.FindElement(By.LinkText("View Report")).Click();

            // Get the current page url
            String url = driverGC.Url;
            // Check if they're equal
            Assert.AreEqual(url, "https://floatnone-stuartjash.azurewebsites.net/Home/PentestingReport");

            // Close & Quit
            driverGC.Close();
            driverGC.Quit();
        }
    }
}
