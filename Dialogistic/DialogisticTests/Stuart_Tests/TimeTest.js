﻿/*
 * As most everything I did this sprint was either API based or done largely
 *    in Javascript, I eneded up writing a Javascipt test to test the Javascript
 *    portion of my code
 *    
 * PBI 222 - Stuart Ashenbrenner
*/

function startTime() {
    // set variables
    var today = new Date();
    var hh = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    var h = hh;
    var ampm = " AM";

    m = checkTime(m);
    s = checkTime(s);

    if (h >= 12) {
        h = hh - 12;
        ampm = " PM";
    }

    if (h == 0) {
        h = 12;
    }


    var currentTime = h + ":" + m + ":" + s + "" + ampm;
    return currentTime;
}
// check the time and if it's less than 10, add a zero in front
function checkTime(i) {
    if (i < 10) {
        // add zero in front of numbers < 10
        i = "0" + i
    };
    return i;
}


var assert = require('assert');
describe('startTime', function () {
    describe('#indexOf()', function () {
        it('Make sure the time ends with an AM or PM', function () {
            var timePeriod = startTime().substr(-2);
            var timeCheck = ("AM" == timePeriod || "PM" == timePeriod) ? true : false;
            assert.equal(timeCheck, true);
        });
    });
});


