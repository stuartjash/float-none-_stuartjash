# README #
Welcome to the repository for Float:None;, a team for the 2018-19 Computer Science Senior Project at Western Oregon University. The main project in this course is meant as a capstone project for a Bachelor's degree in Computer Science.

## Project Contents ##
1. Dialogistic
    * This is the main folder for our group project Dialogistic.
2. Milestones
    * Milestone-1
        - Created resume, letterhead, logo, and project ideas
    * Milestone-2
        - Refined project ideas and initial inception
    * Milestone-3
        - Initial mindmap and some inception ideas
    * Milestone-4
        - Formal user stories, projected ER-Diagram, Site Layout, list of needs & features, non-functional requirements and architecture design, initial modeling, project risks, and timeline with release plan
    * Milestone-5
        - Final additons

## Float:None; Team Members ##
* [Stuart Ashenbrenner](https://github.com/stuartjash)
* [Khorben Boyer](https://github.com/No-one-alone)
* [Dominic Groshong](https://github.com/xzonos)
* [Brock Vance](https://github.com/brockv)

### Team Information ###
* **Team Rules**: The only rule - *you must be willing to go down with the ship*
* **Team Song**: *My Heart Will Go On* by Celine Dion

## Float:None; Project ##
The project that Float:None; has taken on for our senior project is the creation of **Dialogistic**. This web application will help facilitate Phone-A-Thon management.

### Vision Statement ###
>For fundraisers who need phone-a-thon donor data organization and scheduling, Dialogistic is a data management system that will assist in the managing of a phone-a-thon, displaying information on past constituents and potential constituents. It will also allow volunteer fundraisers to be assigned people to call and allow them to submit changes to data records which will be reviewed and approved by an administrator. Unlike the current manual google document process for handling this information and call assignment, our product will allow for CSV importing/exporting and will algorithmically assign calls to volunteers based on history.

### How To Get Started ###
To start using **Dialogistic**, the [starting](starting.md) page will help you fork or clone the repository to your local machine to get you set up as a contributer.

### Contributing ###
To become a contributor, please follow our [Guidelines](guidelines.md). When you contribute to the project, please add your name to our [contributors](contributors.md).

### Software Construction ###
For this project, we are following Agile methodologies, specifically Disciplined Agile Delivery process framework. We follow a two-week sprint cycle that involves daily SCRUM meetings, weekly sprint planning sessions, and end of sprint review and retrospective. To contribute to this project, one will need to be involved in this process with the Float:None; team.

### Tools, Configurations, and Packages ###
Our list of [Tools](tools.md) is a comprehensive list of tools, software, types of packages, and version used during the construction of this project. To avoid any compatability issues, please make sure you use the same tools and configurations.


### Current Deployment ###
You can find a live-version of our site at [Dialogisitic](https://floatnone.azurewebsites.net/)