﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PracticeProject.Models;

namespace PracticeProject.Controllers
{
    public class DiscussionsController : Controller
    {
        private PracticeProjectContext db = new PracticeProjectContext();

        // GET: Discussions
        public ActionResult Index()
        {
            var discussions = db.Discussions.Include(d => d.Post);
            return View(discussions.ToList());
        }




        // added this to get data for discussions page and display it in user specified order
        [HttpGet]
        public ActionResult DiscussionContent( int? id, int? check)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // get post id of dicussion post.
            int postId = db.Discussions.Find(id).PostsID;

            // Get post title
            ViewBag.postTitle = db.Posts.Find(postId).Title;

            // Get post content
            ViewBag.postContent = db.Posts.Find(postId).PostContent;

            // Get post timestamp
            ViewBag.postTime = db.Posts.Find(postId).DateTimeOfPost;


            // Get user id for post
            int userId = db.Posts.Find(postId).UsersID;

            // Get user first name
            ViewBag.firstNameOfPoster = db.Users.Find(userId).FirstName;

            // Get user last name
            ViewBag.lastNameOfPoster = db.Users.Find(userId).LastName;

            IEnumerable<Comment> item;

            if (check == 1)
            {
                // get all comments associated with the specified discussion ID.
                item = db.Comments.Where(comment => comment.DiscussionsID == id)
                               .OrderByDescending(b => b.DateTimeOfCommentCreation)
                              .ToList().AsEnumerable();

            }
            else
            {
                // get all comments associated with the specified discussion ID.
                 item = db.Comments.Where(comment => comment.DiscussionsID == id)
                              .OrderBy(b => b.DateTimeOfCommentCreation)
                              .ToList().AsEnumerable();

            }


            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);

        }





        // GET: Discussions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discussion discussion = db.Discussions.Find(id);
            if (discussion == null)
            {
                return HttpNotFound();
            }
            return View(discussion);
        }

        // GET: Discussions/Create
        public ActionResult Create()
        {
            ViewBag.PostsID = new SelectList(db.Posts, "PostsID", "Title");
            return View();
        }

        // POST: Discussions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DiscussionsID,PostsID,Likes")] Discussion discussion)
        {
            if (ModelState.IsValid)
            {
                db.Discussions.Add(discussion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostsID = new SelectList(db.Posts, "PostsID", "Title", discussion.PostsID);
            return View(discussion);
        }

        // GET: Discussions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discussion discussion = db.Discussions.Find(id);
            if (discussion == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostsID = new SelectList(db.Posts, "PostsID", "Title", discussion.PostsID);
            return View(discussion);
        }

        // POST: Discussions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DiscussionsID,PostsID,Likes")] Discussion discussion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(discussion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostsID = new SelectList(db.Posts, "PostsID", "Title", discussion.PostsID);
            return View(discussion);
        }

        // GET: Discussions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discussion discussion = db.Discussions.Find(id);
            if (discussion == null)
            {
                return HttpNotFound();
            }
            return View(discussion);
        }

        // POST: Discussions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Discussion discussion = db.Discussions.Find(id);
            db.Discussions.Remove(discussion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
