﻿/* Javascript */

// javascript log check
console.log("In Javascript");

function tab(classID) {
  var i;
  var x = document.getElementsByClassName("tab");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none"; 
  }
  document.getElementById(classID).style.display = "block"; 
}

// get id of item
var id = window.location.href.split("/").slice(-1)[0];

document.ready(function () {
    // setting source
    var source = "/Posts/ShowPost/" + id;

    // ajax call function
    var ajax_call = function () {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: source,
            success: displayPosts,
            error: errorOnAjax
        });
    };

    function displayPosts(data) {
        console.log("In displayPosts");
        console.log("post info: " + data);

        var update = "<table class = \"table\"><tbody>";

        $.each(data, function (num, item) {
            console.log("i: " + num);
            console.log("item: " + item);
            update += "<tr>" + "<td>" + item
        });

        // update the page
        $("#tableResponse").html(update);
});

/* Throw error if ajax is unsuccessful */
function errorOnAjax() {
    console.log("Ajax error");
}