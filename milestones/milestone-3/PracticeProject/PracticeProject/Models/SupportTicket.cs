namespace PracticeProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SupportTicket
    {
        [Key]
        public int TicketsID { get; set; }

        [Required]
        [StringLength(30)]
        [DisplayName("First Name:")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(40)]
        [DisplayName("Last Name:")]
        public string LastName { get; set; }

        [Required]
        [StringLength(320)]
        [DisplayName("Email Address:")]
        public string Email { get; set; }

        [Required]
        [StringLength(20)]
        [DisplayName("Select Issue:")]
        public string ReportType { get; set; }


        [StringLength(500)]
        [DisplayName("Discriptive Message:")]
        public string UMessage { get; set; }

        private DateTime date = DateTime.Now;
        [DisplayName("Time Placed")]
        public DateTime TimeSubmited
        {
            get { return date; }
            set { date = value; }
        }
    }
}
