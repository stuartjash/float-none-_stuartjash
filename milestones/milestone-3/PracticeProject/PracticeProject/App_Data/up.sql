﻿-- This creates the tables for the articles database.

-- Creates the table for users.
CREATE TABLE [dbo].[Users]
(
	[UsersID]		                 INT IDENTITY (1,1)		NOT NULL, -- Integer primary key for indexing.
	[FirstName]				         NVARCHAR(48)		    NOT NULL, -- Holds first name of user
	[LastName]	                     NVARCHAR(48)		    NOT NULL, -- Holds last name of user
	[DateTimeOfAccountCreation]      DateTime			    NOT NULL, -- Holds time stamp of when user account was made and submitted.

	CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([UsersID] ASC) -- Prevents table from being dropped if relations with foreign keys are present.
);

GO

-- Creates the table for Posts.
CREATE TABLE [dbo].[Posts]
(
	[PostsID]		          INT IDENTITY (1,1)	NOT NULL, -- Integer primary key for indexing.
	[Title]				      NVARCHAR(48)		    NOT NULL, -- Holds Title of post
	[UsersID]	              INT		            NOT NULL, -- Holds user id.
	[PostContent]		      NVARCHAR(1000)        NOT NULL, -- Holds content of post.
	[DateTimeOfPost]          DateTime			    NOT NULL  -- Holds time stamp of when post was made and submitted.


	CONSTRAINT [PK_dbo.Posts] PRIMARY KEY CLUSTERED ([PostsID] ASC), -- Prevents table from being dropped if relations with foreign keys are present.
	CONSTRAINT [FK_dbo.Posts] FOREIGN KEY (UsersID) REFERENCES [dbo].[Users]([UsersID])  -- Prevents table from being dropped if relations with foreign keys are present.
);

GO

-- Creates the table for Discussions.
CREATE TABLE [dbo].[Discussions]
(
	[DiscussionsID]		      INT IDENTITY (1,1)	NOT NULL, -- Integer primary key for indexing.
	[PostsID]				  INT                   NOT NULL, -- Holds post id.
	[Likes]					  INT                   NOT NULL  -- Holds Like(s) count.


	CONSTRAINT [PK_dbo.Discussions] PRIMARY KEY CLUSTERED ([DiscussionsID] ASC), -- Prevents table from being dropped if relations with foreign keys are present.
	CONSTRAINT [FK_dbo.Discussions] FOREIGN KEY (PostsID) REFERENCES [dbo].[Posts]([PostsID])  -- Prevents table from being dropped if relations with foreign keys are present.
);

GO

-- Creates the table for comments.
CREATE TABLE [dbo].[Comments]
(
	[CommentsID]		           INT IDENTITY (1,1)	NOT NULL, -- Integer primary key for indexing.
	[DiscussionsID]				   INT                  NOT NULL, -- Holds discussion id.
	[UsersID]	                   INT		            NOT NULL, -- Holds user id.
	[CommentContent]               NVARCHAR(1000)		NOT NULL, -- Holds content of comment.
	[DateTimeOfCommentCreation]    DateTime			    NOT NULL, -- Holds time stamp of when comment was made and submitted.

	CONSTRAINT [PK_dbo.Comments] PRIMARY KEY CLUSTERED ([CommentsID] ASC), -- Prevents table from being dropped if relations with foreign keys are present.
	CONSTRAINT [FK_dbo.Comments] FOREIGN KEY (DiscussionsID) REFERENCES [dbo].[Discussions]([DiscussionsID]),  -- Prevents table from being dropped if relations with foreign keys are present.
	CONSTRAINT [FK2_dbo.Comments] FOREIGN KEY (UsersID) REFERENCES [dbo].[Users]([UsersID])  -- Prevents table from being dropped if relations with foreign keys are present.
);

GO

CREATE TABLE [dbo].[SupportTickets]
(
	[TicketsID]		            INT IDENTITY (1,1)		NOT NULL,
	[FirstName]					VARCHAR(30)				NOT NULL,
	[LastName]					VARCHAR(40)				NOT NULL,
	[Email]						VARCHAR(320)			NOT NULL,
	[ReportType]				VARCHAR(20)				NOT NULL,
	[UMessage]					NVARCHAR(500)			,
	[TimeSubmited]			    DateTime			    NOT NULL,

	CONSTRAINT [PK_dbo.SupportTickets] PRIMARY KEY CLUSTERED ([TicketsID] ASC) -- Prevents table from being dropped if relations with foreign keys are present.
);

GO

-- Inserts seed article entries to populate table.
INSERT INTO [dbo].[SupportTickets] (FirstName, LastName, Email, ReportType, UMessage, TimeSubmited) VALUES
						('Bob', 'Ross', 'bob@ross.com', 'Bug', 'I cant make a comment on a discussion.','12/04/2017 09:04:22'),
						('Jack', 'Sparrow', 'greatestpirate@everseen.com', 'Bug', 'Why is the rum gone?', '12/04/2018 10:24:22');

INSERT INTO [dbo].[Users] (FirstName, LastName, DateTimeOfAccountCreation) VALUES
						('Jane', ' Stone', '12/04/2017 09:04:22' ),
						('Tom', 'McMasters', '12/04/2018 10:24:22' ),
						('Otto', 'Vanderwall', '01/04/2019 02:12:01');

INSERT INTO [dbo].[Posts] (Title, UsersID, PostContent, DateTimeOfPost) VALUES
						('How about them rockets?', 2,'I love rockets. I love rockets! I LOVE ROCKETS!!!','12/04/2017 09:04:22' ),
						('Is space the final frontier?', 2, 'Who cares, its there! We should totally go!', '12/04/2018 09:04:22'),
						('Do we need to colonize space?', 3,'I mean on the one hand, MARS!!!, On the other hand, its big, red, dead, and will cost us trillions...', '12/04/2019 09:04:22');

INSERT INTO [dbo].[Discussions] (PostsID, Likes) VALUES
						(1, 3),
						(2, 1),
						(3, 0);

INSERT INTO [dbo].[Comments] (DiscussionsID, UsersID, CommentContent, DateTimeOfCommentCreation) VALUES
						(3, 1,'i dont know man. How does one colonize a cold hell?', '12/04/2017 09:05:22'),
						(2, 3, 'That is not a good enough reason! Unless ... there is loot involved ...','12/04/2018 09:06:22'),
						(2, 1, 'Why is it always loot with you! At least drugs make sense.','12/04/2018 10:06:22'),
						(1, 3, 'Okay who let Rocketman into the drugs again!? Oh well at least he is managing proper sentences ... this time ...','12/04/2017 10:44:03'),
						(1, 1, 'Sigh, I knew I should not have left those special brownies out ...','12/04/2017 10:45:17'),
						(1, 3, 'Oh no! Not those brownies!!! We will be lucky if he snaps out of it in a week if that is what happened ...','12/04/2017 10:46:29');
						
						


GO -- This is an SQL command batch terminator.


--------------------------------------------------------------
--    These are the tables generated by Visual Studio       --
--     and are required for user accounts to function       --
--------------------------------------------------------------

-- Users table copied over from Visual Studio
CREATE TABLE [dbo].[AspNetUsers] (
    [Id]                   NVARCHAR (128) NOT NULL,
    [Email]                NVARCHAR (256) NULL,
    [EmailConfirmed]       BIT            NOT NULL,
    [PasswordHash]         NVARCHAR (MAX) NULL,
    [SecurityStamp]        NVARCHAR (MAX) NULL,
    [PhoneNumber]          NVARCHAR (MAX) NULL,
    [PhoneNumberConfirmed] BIT            NOT NULL,
    [TwoFactorEnabled]     BIT            NOT NULL,
    [LockoutEndDateUtc]    DATETIME       NULL,
    [LockoutEnabled]       BIT            NOT NULL,
    [AccessFailedCount]    INT            NOT NULL,
    [UserName]             NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC),
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
    ON [dbo].[AspNetUsers]([UserName] ASC);


-- Roles table copied over from Visual Studio
CREATE TABLE [dbo].[AspNetRoles] (
    [Id]   NVARCHAR (128) NOT NULL,
    [Name] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex]
    ON [dbo].[AspNetRoles]([Name] ASC);

-- User Claims table copied over from Visual Studio
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [UserId]     NVARCHAR (128) NOT NULL,
    [ClaimType]  NVARCHAR (MAX) NULL,
    [ClaimValue] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserClaims]([UserId] ASC);


-- User Logins table copied over from Visual Studio
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] NVARCHAR (128) NOT NULL,
    [ProviderKey]   NVARCHAR (128) NOT NULL,
    [UserId]        NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserLogins]([UserId] ASC);


-- User Roles table copied over from Visual Studio
CREATE TABLE [dbo].[AspNetUserRoles] (
    [UserId] NVARCHAR (128) NOT NULL,
    [RoleId] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserRoles]([UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[AspNetUserRoles]([RoleId] ASC);
